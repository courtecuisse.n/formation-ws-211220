// Creation d'un service WEB et pas d'une page WEB
package fr.ib.nico.mediatheque2Spring;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthController {
	
	// "http://localhost:9002/health" est l'URL pour voir le "OK" si cela fonctionne
	// Mettre l'adresse de recherche
	@RequestMapping("/health")
	public String getHealth() {
		return "OK";
	}
	
	
}
