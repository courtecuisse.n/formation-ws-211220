//CdService est le modele et CdController est le controlleur 
package fr.ib.nico.mediatheque2Spring;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

// ou @component pour faire comprendre qu'il doit correspondre avec un controlleur
@Service
public class CdService {
	
	private List<Cd> cds;

	public CdService() {
		cds = new ArrayList<Cd>();
	}
	
	int getNombreDeCd() {
		return cds.size();
	}
	
	public void ajoutCd(Cd cd) {
		cds.add(cd);
	}
	
	public List<Cd> getCds(){
		return cds;
	}
	
	public Cd getCd(int n) {
		return cds.get(n);
	}
	
	public void changerTitre(int n, String titre) {
		cds.get(n).setTitre(titre);;
	}
	
}
