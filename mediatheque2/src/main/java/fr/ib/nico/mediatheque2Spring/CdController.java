package fr.ib.nico.mediatheque2Spring;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CdController {
	private CdService cdService;

	// il faut mettre un @Autowired pour dire qu'il doit dialoguer avec un service
	@Autowired
	public void setCdService(CdService cdService) {
		this.cdService = cdService;
	}

	// "produces = MediaType.APPLICATION_XML_VALUE" dit se qu'il produit
	@RequestMapping(path = "/cd/nombre", produces = MediaType.APPLICATION_XML_VALUE, method = RequestMethod.GET)
	public int getNombre() {
		return cdService.getNombreDeCd();
	}

	// @RequestBody, c'est pour dire que le cd est à l'intérieur du corps de la
	// requete
	@RequestMapping(path = "/cd", method = RequestMethod.POST)
	public void ajoute(@RequestBody Cd cd) {
		cdService.ajoutCd(cd);
	}

	@RequestMapping(path = "/cd", method = RequestMethod.GET)
	public List<Cd> getTous() {
		return cdService.getCds();
	}

	// {num} fait en sorte d'autorisé un num
	// @PathVariable("num") permet de dire que l'argument de la fonction doit etre
	// rempli par num
	// num est composé de 1 ou plusieurs chiffres : \\d+
	@RequestMapping(path = "/cd/{num:\\d+}", method = RequestMethod.GET)
	public Cd getUn(@PathVariable("num") int n) {
		return cdService.getCd(n);
	}

	// @RequestBody, c'est pour dire que le cd est à l'intérieur du corps de la
	// requete
	@RequestMapping(path = "/cd/{num:\\d+}/titre", method = RequestMethod.PUT)
	public void ChangerTitre(@PathVariable("num") int n, @RequestBody String titre ) {
		cdService.changerTitre(n, titre);
	}
	
	@RequestMapping(path = "/json/cd", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Cd> getTousJson() {
		return cdService.getCds();
	}

}
