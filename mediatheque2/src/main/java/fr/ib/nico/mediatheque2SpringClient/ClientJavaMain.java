package fr.ib.nico.mediatheque2SpringClient;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class ClientJavaMain {

	public static void main(String[] args) {
		try {
			System.out.println("Client JAVA");
			// Indiquer l'adresse url, création de la connection vers le serveur 
			URL url = new URL ("http://localhost:9002/health");
			// Ouverture de la connection 
			URLConnection conn = url.openConnection();
			// Créer un flux en entrée is pour lire ce que me revoie le is  
			InputStream is = conn.getInputStream();
			// Permet de transformer un fulx texte le flux binaire Objet br est lecteur ligne par ligne 
			// Le is est tranformé en bufferreader
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			// Lecture de la linge et donc reponse est la chaine de caractere 
			String reponse = br.readLine();
			// Affichage de réponse 
			System.out.println("Health : "+reponse);
			br.close();
			
		} catch (Exception ex) {
			System.out.println("Erreur : "+ex);
		}

	}

}
