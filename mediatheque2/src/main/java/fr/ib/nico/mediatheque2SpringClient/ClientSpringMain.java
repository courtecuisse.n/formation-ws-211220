// Pour ouvrir, clic droit run as java applicaiton
package fr.ib.nico.mediatheque2SpringClient;


import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import fr.ib.nico.mediatheque2Spring.Cd;

public class ClientSpringMain {

	public static void main(String[] args) {
		try {
			System.out.println("Client JAVA SPRING");
			// RestTemplate est un client REST
			RestTemplate rt = new RestTemplate();
			// Permet de convertir tout ce qu'il trouve comme chaine 
			String health = rt.getForObject("http://localhost:9002/health", String.class);
			System.out.println("health : "+health);
			
			// Permet de convertir tout ce qu'il trouve comme nombre  
			Integer nbCd =rt.getForObject("http://localhost:9002/cd/nombre", Integer.class);			
			System.out.println("Il y a "+nbCd+" CD dans la mediatheque");
			
			// Envoyer des données à CD
			// Void.class permet de dire qu'il n'y a rien dans la réponse
			Cd cd1 = new Cd("Abbey Road","Beatles", 1966);
			Cd cd2 = new Cd("Expansion","Kekfeu", 2019);
			rt.postForObject("http://localhost:9002/cd", cd1, Void.class);
			rt.postForObject("http://localhost:9002/cd", cd2, Void.class);
			
			// Ne fonctionne pas :
			// List<Cd> cds = rt.getForObject("http://localhost:9002/cd", List<Cd>.class);
			
			// Solution 1 :
			/*
			ParameterizedTypeReference<List<Cd>> ref = new ParameterizedTypeReference<List<Cd>> () {
				ResponseEntity<List<Cd>> cdsEntity = rt.exchange("http://localhost:9002/cd", HttpMethod.GET, null, ref);
				List<Cd> cds = cdsEntity.getBody();
			*/
			
			Cd[] cds = rt.getForObject("http://localhost:9002/cd", Cd[].class);
			
			System.out.println("Tous les CD : ");	
			for (Cd cd:cds) {
				System.out.println(" - "+cd);
			}
			
			// Paramètre requete : "http://localhost:9002/cd?no=0"
			Cd cd0 = rt.getForObject("http://localhost:9002/cd/0", Cd.class);
			System.out.println("Premier CD : "+ cd0);
			
			// Permet ed changer le titre du CD 0
			rt.put("http://localhost:9002/cd/0/titre","Help", String.class);
			
			
			
		} catch (Exception ex) {
			System.out.println("Erreur : "+ex);
		}

	}

}
