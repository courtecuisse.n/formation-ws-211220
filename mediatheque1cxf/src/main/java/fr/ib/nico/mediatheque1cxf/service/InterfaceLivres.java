package fr.ib.nico.mediatheque1cxf.service;

import java.util.Date;

import javax.jws.WebService;

// Il n'y a pas de calcul ou autre dans une interface
// l'interface fait le lien entre le service et le serveur 
@WebService
public interface InterfaceLivres {
	public String getInfos();
	public boolean estEmpruntable(int id);
	public Date getRetour(int id);
	public Livre getLivreDuMois();	
	public Livre[] getLivresDeLAnnee();
}