package fr.ib.nico.mediatheque1cxf.client;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import fr.ib.nico.mediatheque1cxf.service.InterfaceLivres;
import fr.ib.nico.mediatheque1cxf.service.Livre;
import fr.ib.nico.mediatheque1cxf.service.Livres;

public class ClientMain {

	public static void main(String[] args) {
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		factory.setAddress("http://localhost:9001/livres");
		factory.setServiceClass(InterfaceLivres.class);
		factory.getOutInterceptors().add(new LoggingOutInterceptor());
		factory.getInInterceptors().add(new LoggingInInterceptor());
		InterfaceLivres livresServices = factory.create(InterfaceLivres.class);
		System.out.println(livresServices.getInfos());
		System.out.println("Livre 4 empruntable : " + livresServices.estEmpruntable(4));
		try {
			System.out.println("Livre -3 empruntable : " + livresServices.estEmpruntable(-3));
		} catch (Exception ex) {
			System.out.println("Exception : " + ex.getMessage());
		}
		System.out.println("Livre 4 retour : " + livresServices.getRetour(4));
		System.out.println("Livre du mois : "+livresServices.getLivreDuMois());
		Livre[] la = livresServices.getLivresDeLAnnee();
		System.out.println("Livre de mars : "+la[2].getTitre());
	}

}
