package fr.ib.nico.mediatheque1cxf.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.jws.WebService;

@WebService
public class Livres implements InterfaceLivres {
	public String getInfos() {
		return "L'entrée de la biblise trouve rue Victor Hugp";
	}

	public boolean estEmpruntable(int id) {
		if (id < 1)
			throw new IllegalArgumentException("Id doit etre 1 ou plus");
		return false;
	}

	public Date getRetour(int id) {
		// renvoyer la date aujourd'hui plus 10jours
		LocalDate d = LocalDate.now();
		d= d.plusDays(10);
		return Date.from(d.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}

	public Livre getLivreDuMois() {
		Livre l = new Livre("La plus secrete", "M. Sarr", 2021);
		DataSource dataSource = new FileDataSource("imagejeu.png");
		DataHandler dataHandler = new DataHandler(dataSource);
		l.setImage(dataHandler);
		return l;
		
	}
	
	public Livre[] getLivresDeLAnnee() {
		Livre[] livres = new Livre[12];
		for (int i=0; i<12; i++) {
			livres[i] = getLivreDuMois();
		}
		return livres;
	}
	

}
