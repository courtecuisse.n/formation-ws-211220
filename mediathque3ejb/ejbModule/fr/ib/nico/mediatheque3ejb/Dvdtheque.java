package fr.ib.nico.mediatheque3ejb;

import java.time.LocalTime;

import javax.ejb.EJB;
import javax.ejb.Stateless;


@Stateless(name = "Dvds", description = "Opération pour la Dvdtheque")
public class Dvdtheque implements IDvdtheque{
	
	private LocalTime derniereInterrogation;
	private IDvdDAO dvdDAO;

	public String getInfos() {
		return "Nouvelle DVDthèque, ouverte de 10h à 18h.\n Il y a "+dvdDAO.getNombre()+" dvd";
	}

	public boolean ouvertA(LocalTime t) {
		derniereInterrogation = t;
		return t.isAfter(LocalTime.of(10,0)) &&
				t.isBefore(LocalTime.of(18,0));
	}

	public LocalTime getDerniereInterrogation() {
		return derniereInterrogation;
	}	
	
	//@EJB(lookup="ejb:/Mediatheque3Ejb/DvdDAO!fr.ib.nico.mediatheque3ejb.IDvdDAO")
	@EJB(name="DvdDAO")
	public void setDvdDAO(IDvdDAO dvdDAO) {
		this.dvdDAO = dvdDAO;
	}
	
}
