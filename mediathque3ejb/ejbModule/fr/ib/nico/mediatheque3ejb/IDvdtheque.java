package fr.ib.nico.mediatheque3ejb;

import java.time.LocalTime;

import javax.ejb.Remote;

// Permet de dire q
@Remote
public interface IDvdtheque {
	
	public String getInfos();
	public boolean ouvertA(LocalTime t);
	public LocalTime getDerniereInterrogation ();
}
