package fr.ib.nico.mediatheque4web;

import java.time.LocalTime;

import javax.naming.Context;
import javax.naming.InitialContext;

import fr.ib.nico.mediatheque3ejb.Dvd;
import fr.ib.nico.mediatheque3ejb.IDvdDAO;
import fr.ib.nico.mediatheque3ejb.IDvdtheque;

public class DvdsMain {

	public static void main(String[] args) {
		System.out.println("Dvd : client lourd");
		try {
			Context context = new InitialContext();
			// dans la console windows PowerShell du serveur wildfly
			IDvdtheque dvdtheque = (IDvdtheque) context.lookup("ejb:/Mediatheque3Ejb/Dvds!fr.ib.nico.mediatheque3ejb.IDvdtheque");
			System.out.println("Information : "+dvdtheque.getInfos());
			System.out.println("<ouvert à 17h30 ? "+dvdtheque.ouvertA(LocalTime.of(17, 30)));
			// Utiliser DvdDAo pour ajouter des DVDs
			IDvdDAO dvdDAO = (IDvdDAO) context.lookup("ejb:/Mediatheque3Ejb/DvdDAO!fr.ib.nico.mediatheque3ejb.IDvdDAO");
			dvdDAO.ajouter(new Dvd("La course aux jouets",2002));
			dvdDAO.ajouter(new Dvd("Maman, j'ai raté l'avion",1997));
			dvdDAO.ajouter(new Dvd("Super Noel",2004));
			System.out.println("Il y a maintenant : "+dvdDAO.getNombre());
			
			
			context.close();
		} catch (Exception ex) {
			System.err.println(ex);
			ex.printStackTrace();
		}
		

	}

}
