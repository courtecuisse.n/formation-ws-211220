package fr.ib.nico.mediatheque3ejb;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Dvd implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int id;
	private String titre;
	private int annee;
	
	public Dvd() {
		this(null,1990);
	}
	
	public Dvd(String t, int a) {
		this.titre = t;
		this.annee = a;
	}	
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public int getAnnee() {
		return annee;
	}
	public void setAnnee(int annee) {
		this.annee = annee;
	}

	@Override
	public String toString() {
		return "id = " + id + ", titre = " + titre + ", annee = " + annee;
	}

	
}
