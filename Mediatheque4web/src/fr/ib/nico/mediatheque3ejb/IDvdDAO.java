package fr.ib.nico.mediatheque3ejb;

import java.util.List;

import javax.ejb.Remote;

//Permet de dire de se connecter a distance à ce bean (interface ok et jndi dispo pour le client)
@Remote
public interface IDvdDAO {

	public int getNombre();
	public void ajouter(Dvd dvd);
	public Dvd lire( int id);
	public List<Dvd> lireTous();
}
